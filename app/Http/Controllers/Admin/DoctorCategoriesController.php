<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DoctorCategories;
use Yajra\Datatables\Datatables;
use Session;

class DoctorCategoriesController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('permission:access.doctor_categories');
      $this->middleware('permission:access.doctor_categories.edit')->only(['edit', 'update']);
      $this->middleware('permission:access.doctor_categories.create')->only(['create', 'store']);
      $this->middleware('permission:access.doctor_categories.delete')->only('destroy');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
     
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $doctor_categories = DoctorCategories::Where('label', 'LIKE', "%$keyword%")
                ->lower()->paginate($perPage);
        } else {
            $doctor_categories = DoctorCategories::paginate($perPage);
        }

        return view('admin.doctor_categories.index', compact('doctor_categories'));
    }

    public function Datatable()
    {
        $doctor_categories = DoctorCategories::all();
      
        return Datatables::of($doctor_categories)
        ->make(true);
         exit;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {

        return view('admin.doctor_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['label' => 'required']);
        $request->parent_id=0;
       
        $doctor_categories = DoctorCategories::create($request->all());
      
        Session::flash('flash_message', __('DoctorCategories added!'));

        return redirect('admin/doctor_categories/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $doctor_categories = DoctorCategories::where('id',$id)->first();//findOrFail($id);
        if($doctor_categories) {  
          
          
            return view('admin.doctor_categories.show', compact('doctor_categories'));
        } else {
            return redirect('admin/doctor_categories');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
      
        $doctor_categories = DoctorCategories::where('id',$id)->first();
        if($doctor_categories){
          
            return view('admin.doctor_categories.edit', compact('doctor_categories'));
        }else{
            return redirect('admin/doctor_categories');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {

        $this->validate($request, ['label' => 'required']);
       
        $doctor_categories = DoctorCategories::findOrFail($id);
     
        $doctor_categories->update($request->all());
        
        Session::flash('flash_message', __('DoctorCategories updated!'));

        return redirect('admin/doctor_categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        
        DoctorCategories::find($id)->delete();
       
        Session::flash('flash_message', __('Role deleted!'));

        return redirect('admin/doctor_categories');
    }
}
