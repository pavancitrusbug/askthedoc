<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use Yajra\Datatables\Datatables;
use Session;

class PagesController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('permission:access.pages');
      $this->middleware('permission:access.pages.edit')->only(['edit', 'update']);
      $this->middleware('permission:access.pages.create')->only(['create', 'store']);
      $this->middleware('permission:access.pages.delete')->only('destroy');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
     
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $pages = Pages::Where('label', 'LIKE', "%$keyword%")
                ->lower()->paginate($perPage);
        } else {
            $pages = Pages::paginate($perPage);
        }
      
        return view('admin.pages.index', compact('pages'));
    }

    public function Datatable()
    {
        $pages = Pages::all();
       
        return Datatables::of($pages)
        ->make(true);
         exit;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {

        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
      
        $this->validate($request, ['label' => 'required','description' => 'required']);
      
        $pages = Pages::create($request->all());
      
        Session::flash('flash_message', __('Pages added!'));

        return redirect('admin/pages/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $pages = Pages::where('id',$id)->first();//findOrFail($id);
        if($pages) {  
          
          
            return view('admin.pages.show', compact('pages'));
        } else {
            return redirect('admin/pages');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
      
        $pages = Pages::where('id',$id)->first();
        if($pages){
          
            return view('admin.pages.edit', compact('pages'));
        }else{
            return redirect('admin/pages');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {

        $this->validate($request, ['label' => 'required','description' => 'required']);
       
        $pages = Pages::findOrFail($id);
     
        $pages->update($request->all());
        
        Session::flash('flash_message', __('Pages: updated!'));

        return redirect('admin/pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        
        Pages::find($id)->delete();
       
        Session::flash('flash_message', __('Pages: deleted!'));

        return redirect('admin/doctor_categories');
    }
}
