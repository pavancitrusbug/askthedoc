<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;
use Yajra\Datatables\Datatables;
use Session;

class SettingsController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('permission:access.settings');
      $this->middleware('permission:access.settings.edit')->only(['edit', 'update']);
      $this->middleware('permission:access.settings.create')->only(['create', 'store']);
      $this->middleware('permission:access.settings.delete')->only('destroy');
    }
    public function Datatable()
    {
        $roles = Settings::where('status',0);
            return Datatables::of($roles)
            ->make(true);
            exit;

    }
    public function index(Request $request)
    {
     
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $settings = Settings::where('key', 'LIKE', "%$keyword%")->orWhere('value', 'LIKE', "%$keyword%")
                ->lower()->paginate($perPage);
        } else {
            $settings = Settings::paginate($perPage);
        }

        return view('admin.setting.index', compact('settings'));
    }
    public function create()
    {
        return view('admin.setting.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, ['key' => 'required', 'value' => 'required']);
        $settings = Settings::create($request->all());
       
     
        Session::flash('flash_message', __('Settings added!'));

        return redirect('admin/settings/');
    }
     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $settings = Settings::where('id',$id)->first();//findOrFail($id);
        if($settings) {  
            return view('admin.setting.show', compact('settings'));
        } else {
            return redirect('admin/roles');
        }
    }
    
}
