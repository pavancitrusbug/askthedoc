<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testimonials;
use Yajra\Datatables\Datatables;
use Session;

class TestimonialsController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('permission:access.testimonials');
      $this->middleware('permission:access.testimonials.edit')->only(['edit', 'update']);
      $this->middleware('permission:access.testimonials.create')->only(['create', 'store']);
      $this->middleware('permission:access.testimonials.delete')->only('destroy');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
     
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $testimonials = Testimonials::Where('label', 'LIKE', "%$keyword%")
                ->lower()->paginate($perPage);
        } else {
            $testimonials = Testimonials::paginate($perPage);
        }
      
        return view('admin.testimonials.index', compact('testimonials'));
    }

    public function Datatable()
    {
        $testimonials = Testimonials::all();
       
        return Datatables::of($testimonials)
        ->make(true);
         exit;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {

        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['label' => 'required','description' => 'required']);
        $request->parent_id=0;
       
        $testimonials = Testimonials::create($request->all());
      
        Session::flash('flash_message', __('Testimonials added!'));

        return redirect('admin/testimonials/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $testimonials = Testimonials::where('id',$id)->first();//findOrFail($id);
        if($testimonials) {  
          
          
            return view('admin.testimonials.show', compact('testimonials'));
        } else {
            return redirect('admin/testimonials');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
      
        $testimonials = Testimonials::where('id',$id)->first();
        if($testimonials){
          
            return view('admin.testimonials.edit', compact('testimonials'));
        }else{
            return redirect('admin/testimonials');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {

        $this->validate($request, ['label' => 'required','description' => 'required']);
       
        $testimonials = Testimonials::findOrFail($id);
     
        $testimonials->update($request->all());
        
        Session::flash('flash_message', __('Testimonials updated!'));

        return redirect('admin/testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        
        Testimonials::find($id)->delete();
       
        Session::flash('flash_message', __('Testimonials deleted!'));

        return redirect('admin/doctor_categories');
    }
}
