<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('doctor_categories_id');
            $table->string('user_name')->unique();
            $table->tinyInteger('rate_policy')->default(0);
            $table->integer('hourly_rate')->nullable();
            $table->integer('cancellation_policy')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('job_title')->nullable();
            $table->text('job_description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_details');
    }
}
