<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->text('patient_question')->nullable();
            $table->text('doctor_reply')->nullable();
            $table->enum('patient_gander',['male', 'female', 'other'])->nullable();
            $table->enum('response_when',['no_rush', 'soon', 'right_away'])->nullable();
            $table->enum('response_in_detail',['concise', 'avarage', 'in_depth'])->nullable();
            $table->integer('amount')->nullable();
            $table->enum('status',['start', 'continue', 'completed'])->nullable();
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
