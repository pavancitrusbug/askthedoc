<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = new User();
        $users->name='Admin';
        $users->email='users.citrusbug@gmail.com';
        $users->password=bcrypt('123456');
        $users->save();

        $role=Role::where('label','=','SU')->first();
        $users->roles()->attach($role->id);
    }
}
