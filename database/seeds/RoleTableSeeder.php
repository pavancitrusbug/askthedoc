<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = ['0' => ['name' => 'Super Admin','label' => 'SU'],
                '1' => ['name' => 'Admin','label' => 'AU'],
                '2' => ['name' => 'User','label' => 'GU'],
                '3' => ['name' => 'Doctors','label' => 'DU'],];
        foreach ($roles as $value)
        {
            $role = new Role;
            $role->name=$value['name'];
            $role->label=$value['label'];
            $role->save();
        }
    }
}
