@extends('layouts.admin')

@section('title','Edit Role')

@section('content')

<div class="row"> 
    <div class="col-md-12">
        <!-- Horizontal Form --> 
        <div class="card">
            <div class="card-header">
                <strong>Edit </strong> Doctor Categoires # {{$doctor_categories->label}}
                <a href="{{ url('/admin/doctor_categories') }}" title="Back" style="float:right;"><button class="btn btn-warning pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
            </div>
            <div class="card-body card-block">
            {!! Form::model($doctor_categories, [
                'method' => 'PATCH',
                'url' => ['/admin/doctor_categories', $doctor_categories->id],
                'class' => 'form-horizontal',
                'id' => 'edit_doctor_categories_form',
                'autocomplete'=>'off'
            ]) !!}

            @include ('admin.doctor_categories.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
            </div>
                                        
        </div>
        <!-- End Horizontal Form -->
    </div>
</div>

<!-- /.row -->
        
@endsection
@section('js')

<script>
    $('#edit_doctor_categories_form').validate({ // initialize the plugin
        rules: {
          
            label: { required:true},
         
        },
        messages: {
        
            label: { required: "Please enter a label." },
         
        
        },
        submitHandler: function (form) { // for demo
            form.submit();
        }
    });
    $('.parent').change(function (e) {

var $this = $(this),
    parent = $this.data('parent'),
    child = $('.child-' + parent);

if ($this.is(':checked')) {
    child.prop('checked', true);
} else {
    child.prop('checked', false);
}
e.preventDefault();
});

</script>

@endsection