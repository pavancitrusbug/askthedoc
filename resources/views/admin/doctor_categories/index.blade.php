@extends('layouts.admin')

@section('title','Setting List')


@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="table-responsive table--no-card m-b-30">
                <div class="box-header">
                    <h3 class="box-title">Doctor Categories List</h3>
                    @if(Auth::user()->can('access.doctor_categories.create'))
                        <a href="{{ url('/admin/doctor_categories/create') }}" class="btn btn-success btn-sm pull-right"
                            title="Add New Role">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="role-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Label</th>                                
                            <th>Action</th>
                        </tr> 
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>   
                </div>
            </div>   
        </div>
</div>
@endsection
@section('js')
    <script>
        $( document ).ready(function() {
            var edit="{{Auth::user()->can('access.doctor_categories.edit')}}";
            var url="{{ url('/admin/doctor_categories/')}}";
           
            $('#role-table').DataTable({
                processing: true,
                serverSide: true,
                ajax:
                    {
                        'type': 'GET',
                        'url': '{!! route('doctor_categoriestable') !!}',
                        
                    },
                columns: [
                    { "data":"label",},
                   
                  
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var edit="";var data_delete="";var view="";

                            view= " <a href="+url+"/"+o.id+"title='View Role'><button class='btn btn-info btn-xs'><i class='fa fa-eye'aria-hidden='true'></i> View</button></a>";
                            if("{{Auth::user()->can('access.doctor_categories.edit')}}")
                            {
                                edit=" <a href='"+url+"/"+o.id+"/edit'title='Edit Role'><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</button></a>";
                            }
                            if("{{Auth::user()->can('access.doctor_categories.delete')}}")
                            {
                                //data_delete='<form method="POST" action="'+url+'/'+o.id+'"accept-charset="UTF-8" style="display:inline" class = "confirm_delete">{{ method_field("DELETE") }}{{ csrf_field() }}<button type="submit" class="btn btn-danger btn-xs delete" title="Delete User"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></button></form>';
                                //data_delete='<a href="#" id="@applicant.Id"  class="btn btn-outline purple-sharp  uppercase delete-confirmation" data-btn-ok-label="Bəli"  data-toggle="confirmation"  data-placement="right" data-original-title="" title="Are you sure?" aria-describedby="confirmation64993"><i class="fa fa-trash" ></i> Delete</a>';
                                data_delete='<a href="#confirm" class="btn btn-default confirmation-callback">Click me</a>';
                            }                   
                                                            
                            return view+edit+data_delete;
                           
                        }
                    }
                ]
            });
      
         
            $('.confirmation-callback').confirmation({
    onConfirm: function(event, element) { alert('confirm') },
    onCancel: function(event, element) { alert('cancel') }
});



          
        });

    </script>

@endsection

