@extends('layouts.admin')


@section('title','Show Role')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Doctor Categories # {{$doctor_categories->label}}</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/doctor_categories') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>

                    @if($doctor_categories->id != 0)
                        @if(Auth::user()->can('access.doctor_categories.edit'))
                            <a href="{{ url('/admin/doctor_categories/' . $doctor_categories->id . '/edit') }}" title="Edit Doctor Categories">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i>
                                   Edit
                                </button>
                            </a>
                        @endif

                        @if(Auth::user()->can('access.doctor_categories.delete'))
                            {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/doctor_categories', $doctor_categories->id],
                            'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Role',
                                    'onclick'=>"return confirm('Cofirm Delete?')"
                            ))!!}
                            {!! Form::close() !!}
                        @endif
                        
                    @endif


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                           
                            <tr>

                                <td>Label</td>
                                <td> {{ $doctor_categories->label }} </td>
                            </tr>
                           
                         
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection