@extends('layouts.admin')

@section('title','Edit Role')

@section('content')

<div class="row"> 
    <div class="col-md-12">
        <!-- Horizontal Form --> 
        <div class="card">
            <div class="card-header">
                <strong>Edit </strong> pages # {{$pages->label}}
                <a href="{{ url('/admin/pages') }}" title="Back" style="float:right;"><button class="btn btn-warning pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
            </div>
            <div class="card-body card-block">
            {!! Form::model($pages, [
                'method' => 'PATCH',
                'url' => ['/admin/pages', $pages->id],
                'class' => 'form-horizontal',
                'id' => 'edit_pages_form',
                'autocomplete'=>'off'
            ]) !!}

            @include ('admin.pages.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
            </div>
                                        
        </div>
        <!-- End Horizontal Form -->
    </div>
</div>

<!-- /.row -->
        
@endsection
@section('js')

<script>
 CKEDITOR.replace( 'description' );

    $('#edit_pages_form').validate({ // initialize the plugin
        rules: {
           
           label: { required:true,maxlength:30},
           slug: { required:true,maxlength:20},
           description:{required:true}
         
       },
       messages: {
        
           label: { required: "Please enter a label." },
           slug: { required:"Please enter a slug."},
           description:{required:"Please enter a description"}
          
       
       },
        submitHandler: function (form) { // for demo
            form.submit();
        }
    });
    $('.parent').change(function (e) {

var $this = $(this),
    parent = $this.data('parent'),
    child = $('.child-' + parent);

if ($this.is(':checked')) {
    child.prop('checked', true);
} else {
    child.prop('checked', false);
}
e.preventDefault();
});

</script>

@endsection