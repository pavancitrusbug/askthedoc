@extends('layouts.admin')


@section('title','Show Role')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Page # {{$pages->label}}</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/pages') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>

                    @if($pages->id != 0)
                        @if(Auth::user()->can('access.pages.edit'))
                            <a href="{{ url('/admin/pages/' . $pages->id . '/edit') }}" title="Edit Doctor Categories">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i>
                                   Edit
                                </button>
                            </a>
                        @endif

                        @if(Auth::user()->can('access.pages.delete'))
                            {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/pages', $pages->id],
                            'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Role',
                                    'onclick'=>"return confirm('Cofirm Delete?')"
                            ))!!}
                            {!! Form::close() !!}
                        @endif
                        
                    @endif


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                           
                            <tr>

                                <td>Label</td>
                                <td> {{ $pages->label }} </td>
                            </tr>
                            <tr>

                                <td>Description</td>
                                <td> {{ $pages->description }} </td>
                            </tr>
                           
                         
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection