
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="text-input" class=" form-control-label">Name</label>
        </div>
        <div class="col-12 col-md-9">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="text-input" class=" form-control-label">Label</label>
        </div>
        <div class="col-12 col-md-9">
            {!! Form::text('label', null, ['class' => 'form-control']) !!}
            {!! $errors->first('label', '<p class="help-block">:message</p>') !!}     
        </div>
    </div>
    <div class="row form-group">
        <label for="permissions" class="col-md-4 control-label">
            <span class="field_compulsory">*</span>
            Permissions
        </label>
        <div class="col-12 col-md-9">

            <ul class="ul_list_style_n">

                @foreach($permissions as $permission)

                    <li>
                        <div class="checkbox">
                            <input type="checkbox" name="permissions[]" class="parent" id="{{$permission->name}}"
                                                data-parent="{!! $permission->id !!}"
                                                value="{{ $permission->name }}" {!! isset($isChecked)?$isChecked($permission->name):"" !!} >

                            <label for="{{$permission->name}}"> [ {{$permission->name}} ] <span class="text-danger">{{ $permission->label }}</span></label>
                        </div>
                        <ul class="child">
                            @foreach($permission->child as $perm)
                                <li>
                                    <div class="checkbox">
                                    <input type="checkbox" name="permissions[]" id="{{$perm->name}}"
                                                    class="child-{!! $perm->parent_id !!}"
                                                    {!! isset($isChecked)?$isChecked($perm->name):"" !!}
                                                    value="{{ $perm->name }}">
                                        <label for="{{$perm->name}}">
                                            <span class="text-info ">{{ $perm->label }}</span> [ {{$perm->name}} ]
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                @endforeach
            </ul>       
            {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>

