@extends('layouts.admin')

@section('title','Create Role')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <!-- Horizontal Form --> 
        <div class="card">
            <div class="card-header">
                <strong>Create </strong> settings
                <a href="{{ url('/admin/roles') }}" title="Back" style="float:right;"><button class="btn btn-warning pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
            </div>
            <div class="card-body card-block">
                {!! Form::open(['url' => '/admin/settings', 'class' => 'form-horizontal','id' => 'create_settings_form','autocomplete'=>'off']) !!}

                    @include ('admin.setting.form')

                {!! Form::close() !!}
            </div>
                                        
        </div>
        <!-- End Horizontal Form -->
    </div>
</div>

@endsection
@section('js')

<script>
    $('#create_settings_form').validate({ // initialize the plugin
        rules: {
            name: {required: true},
            label: { required:true},
            "permissions[]": {required:true}
        },
        messages: {
            name: { required: "Please enter a role name." },
            label: { required: "Please enter a role label." },
            "permissions[]": {required:"Please Select Any one Permission."}
        
        },
        submitHandler: function (form) { // for demo
            form.submit();
        }
    });
    
    $('.delete').on('click',function(e){
                alert("ok");
                e.preventDefault();
                var form = $(this).parents('form');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Lead !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function(isConfirm){
                    if (isConfirm) {
                                    swal({
                                        title: 'Success!',
                                        type: 'success'
                                    }, function() {
                                        form.submit();
                                    });
                                } else {
                                    swal("Cancel","","error");
                                }
                });
            });

</script>

@endsection