@extends('layouts.admin')

@section('title','Edit Role')

@section('content')

<div class="row"> 
    <div class="col-md-12">
        <!-- Horizontal Form --> 
        <div class="card">
            <div class="card-header">
                <strong>Edit </strong> Role # {{$role->name}}
                <a href="{{ url('/admin/roles') }}" title="Back" style="float:right;"><button class="btn btn-warning pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
            </div>
            <div class="card-body card-block">
            {!! Form::model($role, [
                'method' => 'PATCH',
                'url' => ['/admin/roles', $role->id],
                'class' => 'form-horizontal',
                'id' => 'edit_role_form',
                'autocomplete'=>'off'
            ]) !!}

            @include ('admin.setting.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
            </div>
                                        
        </div>
        <!-- End Horizontal Form -->
    </div>
</div>

<!-- /.row -->
        
@endsection
@section('js')

<script>
    $('#edit_role_form').validate({ // initialize the plugin
        rules: {
            name: {required: true},
            label: { required:true},
            "permissions[]": {required:true}
        },
        messages: {
            name: { required: "Please enter a role name." },
            label: { required: "Please enter a role label." },
            "permissions[]": {required:"Please Select Any one Permission."}
        
        },
        submitHandler: function (form) { // for demo
            form.submit();
        }
    });
    $('.parent').change(function (e) {

var $this = $(this),
    parent = $this.data('parent'),
    child = $('.child-' + parent);

if ($this.is(':checked')) {
    child.prop('checked', true);
} else {
    child.prop('checked', false);
}
e.preventDefault();
});

</script>

@endsection