
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="text-input" class=" form-control-label">Key</label>
        </div>
        <div class="col-12 col-md-9">
            {!! Form::text('key', null, ['class' => 'form-control']) !!}
            {!! $errors->first('key', '<p class="help-block">:message</p>') !!}        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="text-input" class=" form-control-label">Value</label>
        </div>
        <div class="col-12 col-md-9">
            {!! Form::text('value', null, ['class' => 'form-control']) !!}
            {!! $errors->first('value', '<p class="help-block">:message</p>') !!}     
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>

