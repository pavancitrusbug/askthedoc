@extends('layouts.admin')


@section('title','Show Role')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Setting # {{$settings->key}}</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/settings') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>

                    @if($settings->id != 0)
                        @if(Auth::user()->can('access.settings.edit'))
                            <a href="{{ url('/admin/settings/' . $settings->id . '/edit') }}" title="Edit Settings">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i>
                                   Edit
                                </button>
                            </a>
                        @endif

                        @if(Auth::user()->can('access.settings.delete'))
                            {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/settings', $settings->id],
                            'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Role',
                                    'onclick'=>"return confirm('Cofirm Delete?')"
                            ))!!}
                            {!! Form::close() !!}
                        @endif
                        
                    @endif


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                           
                            <tr>

                                <td>Name</td>
                                <td> {{ $settings->key }} </td>
                            </tr>
                            <tr>

                                <td>Label</td>
                                <td> {{ $settings->value }} </td>
                            </tr>
                            
                         
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection