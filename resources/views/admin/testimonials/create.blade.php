@extends('layouts.admin')

@section('title','Create Role')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <!-- Horizontal Form --> 
        <div class="card">
            <div class="card-header">
                <strong>Create </strong> Testimonials
                <a href="{{ url('/admin/testimonials') }}" title="Back" style="float:right;"><button class="btn btn-warning pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
            </div>
            <div class="card-body card-block">
                {!! Form::open(['url' => '/admin/testimonials', 'class' => 'form-horizontal','id' => 'create_testimonials_form','autocomplete'=>'off']) !!}

                    @include ('admin.testimonials.form')

                {!! Form::close() !!}
            </div>
                                        
        </div>
        <!-- End Horizontal Form -->
    </div>
</div>

@endsection
@section('js')

<script>
    $('#create_testimonials_form').validate({ // initialize the plugin
        rules: {
           
            label: { required:true,maxlength:30},
            description:{required:true}
          
        },
        messages: {
         
            label: { required: "Please enter a label." },
            description:{required:"Please enter a description"}
           
        
        },
        submitHandler: function (form) { // for demo
            form.submit();
        }
    });
    
    $('.delete').on('click',function(e){
                alert("ok");
                e.preventDefault();
                var form = $(this).parents('form');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Lead !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function(isConfirm){
                    if (isConfirm) {
                                    swal({
                                        title: 'Success!',
                                        type: 'success'
                                    }, function() {
                                        form.submit();
                                    });
                                } else {
                                    swal("Cancel","","error");
                                }
                });
            });

</script>

@endsection