@extends('layouts.login')

@section('content')

<div class="container">
    <div class="login-wrap">
        <div class="login-content">
            <div class="login-logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="CoolAdmin">
                </a>
            </div>
            <div class="login-form">
            <form method="POST" action="{{ route('login') }}" id="login_form">
                    @csrf
                    <div class="form-group">
                        <label>Email Address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="login-checkbox">
                        
                        <label>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </label>
                    </div>
                    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(e){
		
            $('#login_form').validate({ // initialize the plugin
				rules: {
					email: {required: true,email: true},
					password: { required:true,minlength: 6}
				},
				messages: {
					email: { required: "Please enter a email." },
					password: { required: "Please enter a password." },
				
				},
				submitHandler: function (form) { // for demo
					form.submit();
				}
			});

});
</script>
@endsection
