@extends('layouts.login')

@section('content')

<div class="container">
    <div class="login-wrap">
        <div class="login-content">
            <div class="login-logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="CoolAdmin">
                </a>
            </div>
            
            <div class="login-form">
            @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
            <form method="POST" action="{{ route('password.email') }}" id="forgot_password_data">
                    @csrf
                    <div class="form-group">
                        <label>Email Address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus>

@if ($errors->has('email'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif
                    </div>
                    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Submit</button>
            </form>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function(e){
		
            $('#forgot_password_data').validate({ // initialize the plugin
				rules: {
					email: {required: true,email: true},
					
				},
				messages: {
					email: { required: "Please enter a email." },
				
				},
				submitHandler: function (form) { // for demo
					form.submit();
				}
			});

});
</script>
@endsection
