@extends('layouts.login')

@section('content')

<div class="container">
    <div class="login-wrap">
        <div class="login-content">
            <div class="login-logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="CoolAdmin">
                </a>
            </div>
            <div class="login-form">
                <form method="POST" action="{{ route('password.update') }}" id="reset_password_data">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Reset Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function(e){
		
            $('#reset_password_data').validate({ // initialize the plugin
				rules: {
					email: {required: true,email: true},
					password: { required:true,minlength: 6},
                    password_confirmation: { required:true,minlength: 6, equalTo: "#password"},
					
				},
				messages: {
					email: { required: "Please enter a email." },
					password: { required: "Please enter a Password." },
                    password_confirmation: { required: "Please enter a Confirm Password." },
				
				},
				submitHandler: function (form) { // for demo
					form.submit();
				}
			});

});
</script>
@endsection
