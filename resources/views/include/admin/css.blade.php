    <link href="{{asset('back-end/css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('back-end/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('back-end/vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('back-end/vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />

    <!-- Main CSS-->
    <link href="{{asset('back-end/css/theme.css')}}" rel="stylesheet" media="all">

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" media="all">
      <style >
        .error,.help-block{
            color:red;
        }
    </style>