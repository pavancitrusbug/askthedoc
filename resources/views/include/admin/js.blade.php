<!-- Jquery JS-->
    <script src="{{asset('back-end/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('back-end/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset('back-end/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('back-end/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('back-end/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('back-end/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('back-end/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('back-end/vendor/select2/select2.min.js')}}">
    </script>
    <script src="{{asset('back-end/vendor/bootstrap-confirmation.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset('back-end/js/main.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
     <script>   $( document ).ready(function() {
           
            $('.delete').on('click',function(e){
                alert("ok");
                e.preventDefault();
                var form = $(this).parents('form');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Lead !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function(isConfirm){
                    if (isConfirm) {
                                    swal({
                                        title: 'Success!',
                                        type: 'success'
                                    }, function() {
                                        form.submit();
                                    });
                                } else {
                                    swal("Cancel","","error");
                                }
                });
            });

          
        
        });

    </script>
    @yield('js')