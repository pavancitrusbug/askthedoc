<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/roles')}}">
                                <i class="fas fa-settings-alt"></i>Role
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/permissions')}}">
                                <i class="fas fa-settings-alt"></i>Permission
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/settings')}}">
                                <i class="fas fa-settings-alt"></i>Settings
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/doctor_categories')}}">
                                <i class="fas fa-settings-alt"></i>Doctor Categories
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/testimonials')}}">
                                <i class="fas fa-settings-alt"></i>Testimonials
                            </a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="{{url('admin/pages')}}">
                                <i class="fas fa-settings-alt"></i>Pages
                            </a>
                        </li>
                       
                    </ul>
                </nav>
            </div>
        </aside>