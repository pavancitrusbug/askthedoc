<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin','middleware' => ['auth'],'name' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index');
    Route::get('role-data', ['as' => 'roletable', 'uses' => 'Admin\RolesController@Datatable']);
    Route::resource('/roles', 'Admin\RolesController');
    Route::get('settings-data', ['as' => 'settingstable', 'uses' => 'Admin\SettingsController@Datatable']);
    Route::resource('/settings', 'Admin\SettingsController');
    Route::get('permissions/datatable', 'Admin\PermissionsController@datatable');
    Route::resource('/permissions', 'Admin\PermissionsController');
    Route::get('doctor_categories-data', ['as' => 'doctor_categoriestable', 'uses' => 'Admin\DoctorCategoriesController@Datatable']);
    Route::resource('/doctor_categories', 'Admin\DoctorCategoriesController');
    Route::get('testimonials-data', ['as' => 'testimonialstable', 'uses' => 'Admin\TestimonialsController@Datatable']);
    Route::resource('/testimonials', 'Admin\TestimonialsController');
    Route::get('pages-data', ['as' => 'pagestable', 'uses' => 'Admin\PagesController@Datatable']);
    Route::resource('/pages', 'Admin\PagesController');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
